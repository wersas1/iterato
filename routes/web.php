<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', '\App\Http\Controllers\Welcome');

Route::get('/admin', '\App\Http\Controllers\ShowAdmin');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/token', [App\Http\Controllers\HomeController::class, 'storeKey'])->name('user.token');

Route::resource('/cities', App\Http\Controllers\CitiesController::class);
Route::get('/admin/cities', [App\Http\Controllers\CitiesController::class, 'admin'])->name('cities.admin');

Route::resource('/users', App\Http\Controllers\UsersController::class);

Auth::routes();

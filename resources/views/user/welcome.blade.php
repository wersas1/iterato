@extends('layouts.app')
@section('title', 'Weather Forecast')
@section('content')
    <div class="container">
        <div class="p-6">
            <div class="mx-auto">
                <div class="row justify-content-center">
                    <div class="col-sm-10">
                        <div class="flex items-center welcome-message">
                            <div class="mx-auto" id="pt-unique">
                                <h1 class="mx-5">Weather Forecast System</h1>
                                <p class="mx-5">
                                    Welcome to the weather forecast system. If you are not a registered user please
                                    register by clicking the register button at the top right for full experience.
                                    If you do not want to register you can proceed by viewing existing forecasts <a
                                        href="{{url('/cities')}}">here</a>.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

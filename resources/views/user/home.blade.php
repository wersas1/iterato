@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0 py-0 my-0">{{ __('User Control Panel') }}</div>

                    <div class="card-body bg-transparent border-0 py-2">

                        <div class="container-fluid">
                            <div class="container mx-auto">
                                <form method="POST" action="{{route('user.token')}}">
                                    @csrf
                                    <div class="form-group row">
                                        <small class="small text-justify mx-auto py-4 text-right"
                                               style="font-size:12px;padding-left:130px">
                                            Store your API key here. @if($user->api_key==null) Get it from <a
                                                style="font-size:12px;"
                                                href="https://openweathermap.org/guide#how">here</a> first. @else You make another one <a
                                                style="font-size:12px;"
                                                href="https://openweathermap.org/guide#how">here</a>. @endif</small>

                                    </div>
                                    <div class="form-group row pb-2">
                                        <label for="user_api_key"
                                               class="col-md-4 col-form-label text-md-right">{{ __('API Key') }}</label>
                                        <div class="col-md-8 mx-0 px-0">
                                            <div class="d-inline-flex input-wrapper">
                                                <input id="user_api_key" type="text"
                                                       class="form-control @error('user_api_key') is-invalid @enderror"
                                                       name="user_api_key"
                                                       value="@if($user->api_key){{$user->api_key}}@else{{ old('user_api_key') }}@endif"
                                                       required
                                                       autocomplete="user_api_key" autofocus>
                                                <button type="submit" class="btn btn-success btn-submit"><i
                                                        class="fas fa-check" style="font-size:18px;"></i></button>
                                            </div>
                                            @error('user_api_key')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

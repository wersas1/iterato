@extends('layouts.app')
@section('title', 'Admin Control Panel')
@section('content')
    <div class="container-fluid">
        <div class="container mx-auto">
            <div class="card">
                <div class="card-header text-dark"><h2>Admin Control panel</h2></div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-5 button-pan my-2 mx-1 px-0">
                            <div class="card main-card">
                                <a href="{{url('/admin/cities')}}" class="text-dark">
                                    <div class="card-body">
                                        <div class="container-fluid mt-2 mb-2"
                                             style="width:100%;text-align:center;">
                                            <i class="fa fa-city"
                                               style="font-size:38px;"></i>
                                        </div>
                                        <div class="card-text" style="text-align:center;">
                                            <span>Cities</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-5 button-pan my-2 mx-1 px-0">
                            <div class="card main-card">
                                <a href="{{url('/users')}}" class="text-dark">
                                    <div class="card-body">
                                        <div class="container-fluid mt-2 mb-2"
                                             style="width:100%;text-align:center;">
                                            <i class="fa fa-users-cog"
                                               style="font-size:38px;"></i>
                                        </div>
                                        <div class="card-text" style="text-align:center;">
                                            <span>Users</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Weathers Page') }} | @yield('title')</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
    <script>
        $(window).on('load', function () {
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!--FA-->
    <script src="https://kit.fontawesome.com/c3927129d9.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
<div class="se-pre-con"></div>

<div id="app">
    <div class="container-fluid">
        <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-transparent mx-2 py-2">

            <a class="navbar-brand d-inline-flex" style="margin-top:-5px;" href="{{url('/')}}"><img
                    alt="Weather Forecast logo"
                    src="{{asset('/img/logo.png')}}"
                    style="margin-left: 115px;margin-right:25px;width: 120px;height: 120px;"/>
                <h1 class="mt-4 pt-4">Weathers Page | @yield('title')</h1></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-right:115px;">
                <ul class="navbar-nav ml-auto my-2 my-lg-0 semi-bold" style="margin-top: 58px;">
                    <li class="nav-item mx-2 mt-1">
                        <a class="nav-link text-light" href="{{url('/cities')}}">Cities</a>
                    </li>
                    @auth
                        <li class="nav-item dropdown  mx-2 mt-1">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{url('/home/')}}">
                                    User Control Panel
                                </a>
                                @if (Auth::user()->type>=1)
                                    <a class="dropdown-item" href="{{url('/admin/')}}">
                                        Admin Control Panel
                                    </a>
                                @endif
                            </div>
                        </li>
                    @else
                        <li class="nav-item mx-2 mt-1">
                            <a class="nav-link text-light" href="{{ route('login') }}">
                                Login
                            </a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item mx-2 mt-1">
                                <a class="nav-link text-light" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @endauth

                </ul>
            </div>
        </nav>
        <!--/.Navbar-->
    </div>
    <div class="container-fluid mx-auto" id="status_container" style="text-align:center;">
        @if (session('alert'))
            <div onclick="terminateAlert();" style="width:30%;margin-left:auto;margin-right:auto;text-align:center;"
                 id="alert" class="alert alert-dark bg-dark text-light">
                {{ session('alert') }}
            </div>
        @elseif (session('error'))
            <div onclick="terminateAlert();" style="width:30%;margin-left:auto;margin-right:auto;text-align:center;"
                 id="error" class="alert alert-dark bg-dark text-light">
                {{ session('error') }}
            </div>
        @elseif (session('status'))
            <div onclick="terminateAlert();" style="width:30%;margin-left:auto;margin-right:auto;text-align:center;"
                 id="alert" class="alert alert-dark bg-success text-light">
                {{ session('status') }}
            </div>
        @elseif(session('success'))
            <div onclick="terminateAlert();" style="width:30%;margin-left:auto;margin-right:auto;text-align:center;"
                 id="alert" class="alert alert-dark bg-success text-light">
                {{ session('success') }}
            </div>
        @endif
    </div>
    <main class="py-3 my-3">
        @yield('content')
    </main>
</div><!--Wrapper Ends-->
<footer
    class="page-footer font-small mdb-color text-dark darken-3 pt-2 footer mx-auto border-top border-muted"
    style="background-color: #fff;">
    <div class="container mx-auto">
        <div class="row d-flex justify-content-center mx-auto">
            <div class="col-md-11 d-block">
                Let me know if you encounter any problems:
                <br/>
                <a class="text-dark" href="tel:+37064711326">+370 647 11 326</a>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center text-dark py-3" style="font-size:12px;">© {{date("Y")}} All rights
        reserved:
        <a class="text-dark footer-copyright" href="mailto:v.baubonis@gmail.com"> v.baubonis@gmail.com</a>
    </div>
</footer>
<script src="{{asset('/js/app.js')}}" defer type="text/javascript"></script>
<script type="text/javascript">
    function terminateAlert() {
        var x = document.getElementById("alert");
        x.remove();
    }
</script>
</body>
</html>

@extends('layouts.app')
@section('title', 'Users list')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0">{{ __('Users list') }}</div>

                    <div class="card-body bg-transparent border-0">
                        <table class="table table-striped text-light">
                            <thead>
                            <tr>
                                <th scope="col">
                                    Type
                                </th>
                                <th scope="col">
                                    Email
                                </th>
                                <th>
                                    API_Key
                                </th>
                                <th scope="col">
                                    Actions (View, Edit, Delete)
                                </th>
                            </tr>
                            <tr>
                                <th colspan="4">{{$users->links()}} <a href="{{url('/users/create')}}">Create New
                                        User</a></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th>@if($user->type==0) Registered user @else Administrator @endif</th>
                                    <td>
                                        {{$user->email}}
                                    </td>
                                    <td>
                                        @isset($user->api_key) {{$user->api_key}} @else N/A @endif
                                    </td>
                                    <td class="text-center d-inline-flex mx-auto vertical-center"
                                        style="margin-bottom:1px;position:relative;bottom:1px">
                                        <a style="width:60px;height:auto;" class="btn btn-success  my-2 py-2 mx-1"
                                           href="{{url('/users/' . $user->id)}}"><i class="fas fa-eye"></i></a>
                                        <a style="width:60px;height:auto;" class="btn btn-warning  my-2 py-2 mx-1"
                                           href="{{url('/users/' . $user->id.'/edit')}}"><i class="fas fa-edit"></i></a>
                                        <form action="{{route('users.destroy',$user->id) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button data-toggle="confirmation" style="width:60px;height:auto;"
                                                    class="btn btn-danger mx-auto my-2 py-2"><i
                                                    class="fas fa-times"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

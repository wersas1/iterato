@extends('layouts.app')
@section('title', 'Show user')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0"><h2>{{ __('User') }}</h2></div>
                    <div class="card-body bg-transparent border-0">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th scope="row">{{ucfirst('name')}} </th> <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <th scope="row">{{ucfirst('email')}} </th> <td>{{$user->email}}</td>
                            </tr>
                            @isset($user->api_key)
                            <tr>
                                <th scope="row">{{ucfirst('api key')}} </th> <td>{{$user->api_key}}</td>
                            </tr>
                            @endisset
                            @isset($user->created_at)
                            <tr>
                                <th scope="row">{{ucfirst('User registered on')}} </th> <td>{{$user->created_at}}</td>
                            </tr>
                            @endisset
                            @isset($user->updated_at)
                                <tr>
                                    <th scope="row">{{ucfirst('User last updated on')}} </th> <td>{{$user->updated_at}}</td>
                                </tr>
                            @endisset
                            <tr>
                                <th scope="row">User Type </th> <td>@if($user->type==0) Member @else Administrator @endif </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('title', 'Cities list')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0">
                        <h2 class="">{{ __('Cities list') }}</h2>
                    </div>
                    <div class="card-body bg-transparent border-0">
                        <ul class="nav nav-tabs" id="citiesTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link btn-outline-primary text-warning active" id="0-tab" data-toggle="tab"
                                   href="#nav-0" role="tab" aria-controls="nav-0" aria-selected="true">Add city</a>
                            </li>
                            @foreach($cities as $city)
                                @auth()
                                    @if($user->id==$city->user_id)
                                        <li class="nav-item">
                                            <a class="nav-link btn-outline-primary text-light" id="{{$city->id}}-tab"
                                               data-toggle="tab"
                                               href="#nav-{{$city->id}}" role="tab" aria-controls="nav-{{$city->id}}"
                                               aria-selected="true">{{$city->name}}</a>
                                        </li>
                                    @endif
                                @else
                                    @if($city->user_id==null)
                                        <li class="nav-item">
                                            <a class="nav-link btn-outline-primary text-light" id="{{$city->id}}-tab"
                                               data-toggle="tab"
                                               href="#nav-{{$city->id}}" role="tab" aria-controls="nav-{{$city->id}}"
                                               aria-selected="true">{{$city->name}}</a>
                                        </li>
                                    @endif
                                @endauth
                            @endforeach

                        </ul>
                        <div class="tab-content my-5 border-info border" id="citiesTabContent">
                            <div class="tab-pane @auth mb-5 @endauth fade show active" id="nav-0" role="tabpanel"
                                 aria-labelledby="0-tab">
                                @auth
                                    <form method="POST" style="margin-right:10%;">
                                        @csrf
                                        <div class="form-group row">
                                            @if($user->api_key==null)
                                                <small class="small text-justify mx-auto py-4"
                                                       style="font-size:12px;padding-left:15%;">
                                                    Before proceeding with the form you must get the API key from <a
                                                        style="font-size:12px;"
                                                        href="https://openweathermap.org/guide#how">here</a>.</small>
                                            @else
                                                <div class=" mx-auto py-4">
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group row pb-2">
                                            <label for="api_token"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('API Key') }}</label>
                                            <div class="col-md-6">
                                                <input id="api_token" type="text"
                                                       class="form-control @error('api_token') is-invalid @enderror"
                                                       name="api_token"
                                                       value="@if($user->api_key){{$user->api_key}}@else{{ old('api_token') }}@endif"
                                                       required
                                                       autocomplete="api_token" autofocus>
                                                @error('api_token')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="city_name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                                            <div class="col-md-6">
                                                <div class="d-inline-flex input-wrapper">
                                                    <input id="city_name" type="text"
                                                           class="form-control @error('city_name') is-invalid @enderror"
                                                           name="target" value="{{ old('city_name') }}" required
                                                           autocomplete="city_name"
                                                    >
                                                    <button id="city" type="button" class="btn btn-success btn-submit">
                                                        <i
                                                            class="fas fa-check" style="font-size:18px;"></i></button>
                                                </div>
                                                @error('city')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="my-3 text-dark text-center">
                                                <a class="page-link" href="{{route('login')}}"> Please log into the
                                                    system to add new cities.</a>
                                            </p>
                                        </div>
                                    </div>
                                @endauth
                            </div>
                            @foreach($cities as $city)
                                @auth
                                    @if($user->id==$city->user_id)
                                        <div class="tab-pane fade show" id="nav-{{$city->id}}" role="tabpanel"
                                             aria-labelledby="{{$city->id}}-tab">
                                            <table class="table bg-white table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="rowgroup" colspan="4">
                                                        <h3 class="border-bottom pb-3 pt-2"> City: {{$city->name}}</h3>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        Weather & Temperature
                                                    </th>
                                                    <th>
                                                        Min/Max temperature
                                                    </th>
                                                    <th>
                                                        Humidity
                                                    </th>
                                                    <th>
                                                        Feels naturally like
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @isset($city->weather)
                                                    <tr>
                                                        <th> {{ucfirst($city->weather)}} ({{$city->temp}}
                                                            )<span>&#8451;</span>
                                                        </th>
                                                        <td> {{$city->temp_min}}
                                                            <span>&#8451;</span>, {{$city->temp_max}}
                                                            <span>&#8451;</span>
                                                        </td>
                                                        <td>
                                                            {{$city->humidity}} <span>&#37;</span>
                                                        </td>
                                                        <td>
                                                            {{$city->feeling}}<span>&#8451;</span>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="4"><span>No information was located about this city. Try adding a new one <a
                                                                    href="#nav-0" class="home-selector"
                                                                    data-toggle="tab"
                                                                    aria-controls="nav-0"
                                                                    aria-selected="true">here</a>.</span>
                                                        </td>
                                                    </tr>
                                                @endisset
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                @else
                                    @if($city->user_id==null)
                                        <div class="tab-pane fade show" id="nav-{{$city->id}}" role="tabpanel"
                                             aria-labelledby="{{$city->id}}-tab">
                                            <table class="table bg-white table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="rowgroup" colspan="4">
                                                        <h3 class="border-bottom pb-3 pt-2"> City: {{$city->name}}</h3>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        Weather & Temperature
                                                    </th>
                                                    <th>
                                                        Min/Max temperature
                                                    </th>
                                                    <th>
                                                        Humidity
                                                    </th>
                                                    <th>
                                                        Feels naturally like
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @isset($city->weather)
                                                    <tr>
                                                        <th> {{ucfirst($city->weather)}} ({{$city->temp}}
                                                            )<span>&#8451;</span>
                                                        </th>
                                                        <td> {{$city->temp_min}}
                                                            <span>&#8451;</span>, {{$city->temp_max}}
                                                            <span>&#8451;</span>
                                                        </td>
                                                        <td>
                                                            {{$city->humidity}} <span>&#37;</span>
                                                        </td>
                                                        <td>
                                                            {{$city->feeling}}<span>&#8451;</span>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="4"><span>No information was located about this city. Try adding a new one <a
                                                                    href="#nav-0" class="home-selector"
                                                                    data-toggle="tab"
                                                                    aria-controls="nav-0" aria-selected="true">here</a>.</span>
                                                        </td>
                                                    </tr>
                                                @endisset
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                @endauth
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

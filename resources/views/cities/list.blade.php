@extends('layouts.app')
@section('title', 'City list')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0">{{ __('City list') }}</div>
                    <div class="card-body bg-transparent border-0">
                        <table class="table table-striped text-light">
                            <thead>
                            <tr>
                                <th scope="col">
                                    Name
                                </th>
                                <th scope="col">
                                    API Key
                                </th>
                                <th>
                                    Owner
                                </th>
                                <th scope="col">
                                    Actions (View, Delete)
                                </th>
                            </tr>
                            <tr>
                                <th colspan="4">{{$cities->links()}} <a href="{{url('/cities')}}">Create New City</a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($cities->count()>0)
                            @foreach($cities as $city)
                                <tr id="{{$city->id}}">
                                    <td>
                                        {{$city->name}}
                                    </td>
                                    <td>
                                        {{$city->api_key}}
                                    </td>
                                    <td>
                                        @isset($city->user)   {{$city->user->name}} @else N/a @endisset
                                    </td>
                                    <td class="text-center d-inline-flex mx-auto vertical-center"
                                        style="margin-bottom:1px;position:relative;bottom:1px">
                                        <a style="width:60px;height:auto;" class="btn btn-success  my-2 py-2 mx-1"
                                           href="{{url('/cities/' . $city->id)}}"><i class="fas fa-eye"></i></a>
                                        <a style="width:60px;height:auto;" class="btn btn-warning  my-2 py-2 mx-1"
                                           href="{{url('/cities/' . $city->id.'/edit')}}"><i
                                                class="fas fa-edit"></i></a>
                                        <button data-toggle="confirmation" style="width:60px;height:auto;"
                                                data-attribute="{{$city->id}}"
                                                class="btn btn-danger destroy-city-btn mx-auto my-2 py-2"><i
                                                class="fas fa-times"></i></button>

                                    </td>
                                </tr>
                            @endforeach
                                @else
                            <tr>
                                <td colspan="4">No cities were found, add one!</td>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

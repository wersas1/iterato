@extends('layouts.app')
@section('title', 'Edit City '.$city->name)
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0">{{ __('Edit City') }}</div>

                    <div class="card-body bg-transparent border-0">
                        <form method="POST" action="{{ route('cities.update', $city->id) }}">
                            @csrf
                            @method('patch')

                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ __('City name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="@if(old($city->city_name)){{old('name')}}@else{{$city->name}}@endif"
                                           autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="api_key"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Api Token') }}</label>

                                <div class="col-md-6">
                                    <input id="api_key" type="text"
                                           class="form-control @error('api_key') is-invalid @enderror"
                                           name="api_key"
                                           value="@if(old($city->api_key)){{old('api_key')}}@else{{$city->api_key}}@endif"
                                           required autocomplete="api_key">

                                    @error('api_key')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_id"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Entry owner') }}</label>

                                <div class="col-md-6">
                                    <select id="user_id"
                                            class="form-control selectpicker @error('user_id') is-invalid @enderror"
                                            name="user_id"
                                            required>
                                        <option @if(!$city->user_id)selected @endif value="">
                                            Global (shown for unauthenticated)
                                        </option>
                                        @foreach($users as $user)
                                            <option @if($city->user_id==$user->id)selected @endif value="{{$user->id}}">
                                                {{$user->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="button" id="update_city" data-attribute="{{$city->id}}" class="btn btn-primary">
                                        {{ __('Edit City') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

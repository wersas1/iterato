require('./bootstrap');

$("#city").on('click', function () {
    createCity()
});

$("#update_city").on('click', function (e) {
    let id = document.getElementById('update_city').getAttribute('data-attribute');
    updateCity(id);
});

$(".home-selector").on('click', function () {
    markHomeTab()
});

$(".destroy-city-btn").on('click', function (e) {
   let id = $(e.target).data('attribute');
   destroyCity(id);
});

function destroyCity(id) {
    $('#'+id).remove();
    fetch('http://localhost/cities/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    })
        .then((resp) => resp.text()).then(function (data) {
        return showAlert(`Successfully deleted a city!`, `bg-success`);
    }).catch(function (error) {
        showAlert(error, `bg-danger`);
    })
}

function markHomeTab() {
    let home = document.getElementById('#0-tab');
    home.classList.add('active');
}

function createCity() {
    let request_data = {
        api_token: document.getElementById('api_token').value,
        city_name: document.getElementById('city_name').value,
    };

    fetch('http://localhost/cities', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        body: JSON.stringify(request_data)
    })
        .then((resp) => resp.json()).then(function (data) {
        return [data].map(function (item) {
            if (item.api_token && item.city_name) {
                showAlert(`${item.api_token} ${item.city_name}`);
            } else if (item.api_token) {
                showAlert(`${item.api_token}`);
            } else if (item.city_name) {
                showAlert(`${item.city_name}`);
            } else if (item.message === "city not found") {
                showAlert(`The city was not found. :(`);
            } else {
                showAlert(`Successfully added a city!`, `bg-success`);
                let li = createNode('li'),
                    a = createNode('a'),
                    ul = document.getElementById('citiesTab'),
                    div = document.getElementById('citiesTabContent'),
                    content = createNode('div'),
                    forecast = item.data;
                console.log(forecast);
                li.classList.add('nav-item');
                a.classList.add('nav-link');
                a.classList.add('btn-outline-primary');
                a.classList.add('text-light');
                a.setAttribute('data-toggle', 'tab');
                a.setAttribute('href', '#nav-' + `${item.id}`);
                a.setAttribute('role', 'tab');
                a.setAttribute('aria-controls', 'nav-' + `${item.id}`);
                a.innerHTML = `${item.name}`;
                content.classList.add('tab-pane');
                content.classList.add('fade');
                content.classList.add('show');
                content.setAttribute('role', 'tabpanel');
                content.setAttribute('aria-labelledby', `${item.id}` + '-tab');
                content.setAttribute('id', 'nav-' + `${item.id}`);
                content.innerHTML = '<table class="table bg-white table-striped table-bordered">\n' +
                    '                                        <thead>\n' +
                    '                                        <tr>\n' +
                    '                                            <th scope="rowgroup" colspan="4">\n' +
                    '                                                <h3 class="border-bottom pb-3 pt-2"> City: ' + `${item.name}` + '</h3>\n' +
                    '                                            </th>\n' +
                    '                                        </tr>\n' +
                    '                                        <tr>\n' +
                    '                                            <th>\n' +
                    '                                                Weather & Temperature\n' +
                    '                                            </th>\n' +
                    '                                            <th>\n' +
                    '                                                Min/Max temperature\n' +
                    '                                            </th>\n' +
                    '                                            <th>\n' +
                    '                                                Humidity\n' +
                    '                                            </th>\n' +
                    '                                            <th>\n' +
                    '                                                Feels naturally like\n' +
                    '                                            </th>\n' +
                    '                                        </tr>\n' +
                    '                                        </thead>\n' +
                    '                                        <tbody>\n' +
                    '                                        <tr>\n' +
                    '                                            <th> ' + `${forecast.weather[0].main}` + ' ' + `${forecast.main.temp}` + ' <span>&#8451;</span></th>\n' +
                    '                                            <td> ' + `${forecast.main.temp_min}` + ' <span>&#8451;</span>,  ' + `${forecast.main.temp_max}` +
                    '                                            <span>&#8451;</span>\n' +
                    '                                            </td>\n' +
                    '                                            <td>\n' +
                    '                                                ' + `${forecast.main.humidity}` + ' <span>&#37;</span>\n' +
                    '                                            </td>\n' +
                    '                                            <td>\n' +
                    '                                                ' + `${forecast.main.feels_like}` + '<span>&#8451;</span>\n' +
                    '                                            </td>\n' +
                    '                                        </tr>\n' +
                    '                                        </tbody>\n' +
                    '                                    </table>';
                append(li, a);
                append(ul, li);
                append(div, content);
            }
        })
    }).catch(function (error) {
        showAlert(error, `bg-danger`);
    })
}

function updateCity(id) {
    let request_data = {
        api_token: document.getElementById('api_key').value,
        city_name: document.getElementById('name').value,
    };

    fetch('http://localhost/cities/' + id, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        body: JSON.stringify(request_data)
    })
        .then((resp) => resp.json()).then(function (data) {
        return [data].map(function (item) {
            if (item.api_token && item.city_name) {
                showAlert(`${item.api_token} ${item.city_name}`);
            } else if (item.api_token) {
                showAlert(`${item.api_token}`);
            } else if (item.city_name) {
                showAlert(`${item.city_name}`);
            } else if (item.message === "city not found") {
                showAlert(`The city was not found. :(`);
            } else {
                showAlert(`Successfully updated a city!`, `bg-success`);
            }
        })
    }).catch(function (error) {
        showAlert(error, `bg-danger`);
    })
}

function createNode(element) {
    return document.createElement(element);
}

function append(parent, el) {
    return parent.appendChild(el);
}

function showAlert(trigger, background = null) {
    let status = createNode('div'),
        message = createNode('span'),
        container = document.getElementById('status_container');
    message.innerHTML = trigger;
    status.classList.add('alert');
    if (background === 'bg-success') status.classList.add('bg-success');
    else status.classList.add('bg-danger');
    status.classList.add('alert');
    status.setAttribute("id", "alert");
    status.setAttribute("style", "width:40%;margin-left:auto;margin-right:auto;padding 20px;");
    status.setAttribute("onClick", 'terminateAlert();');
    let currentAlert = document.getElementById('alert');
    if (currentAlert) terminateAlert();
    append(status, message);
    append(container, status)
}


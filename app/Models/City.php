<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'user_id', 'api_key', 'provider'];

    /**
     * Eloquent relation to user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Recursive function to print response data
     *
     * @param $array
     * @return array
     */
    function print_array(array $array)
    {
        foreach ($array as $key => $value) {
            if (!is_array($value))
              $array= compact('key','value');
            else {
              $array= $this->print_array($value);
            }
        }
        return $array;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use App\Models\City;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class CitiesController extends Controller
{

    /**
     * Show the weather forecasts for the City resources.
     *
     * @return Factory|View
     */
    public function index()
    {
        $user = Auth::user();
        $cities = \App\Models\City::all();
        foreach ($cities as $city) {
            $response = Http::get('http://api.openweathermap.org/data/2.5/weather?q=' . $city->name . '&appid=' . $city->api_key . '&units=metric');
            $data = $response->json();
            if ($response->status() == 200) {
                $city->weather = $data['weather'][0]['description'];
                $city->temp = $data['main']['temp'];
                $city->temp_min = $data['main']['temp_min'];
                $city->temp_max = $data['main']['temp_max'];
                $city->humidity = $data['main']['humidity'];
                $city->feeling = $data['main']['feels_like'];
            }
        }
        return view('cities.forecast', compact('cities', 'user'));
    }

    /**
     * Show the listing of a resource.
     *
     * @return void|View
     */
    public function admin()
    {
        if (Gate::allows('admin-actions')) {
            $cities = \App\Models\City::orderBy('id', 'asc')->paginate(10);
            return view('cities.list', compact('cities'));
        } else return abort(403, 'You are not authorized');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View|void
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        $users = User::all();
        $this->authorize('update', $city);
        return view('cities.edit', compact('city', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return false|string
     */
    public function store(Request $request)
    {
        if (Gate::allows('logged-in')) {
            $data = $request->all();
            $rules = [
                'city_name' => 'required|unique:App\Models\City,name',
                'api_token' => 'required|min:2|max:64',
                'provider' => 'nullable',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                $response = Http::get('http://api.openweathermap.org/data/2.5/weather?q=' . $request->get('city_name') . '&appid=' . $request->get('api_token') . '&units=metric');
                if ($response->status() === 200) {
                    $city = new City;
                    $city->api_key = $request->get('api_token');
                    $city->name = $request->get('city_name');
                    $city->user_id = Auth::user()->id;
                    if ($provider_exists = $request->get('provider')) $city->provider = $provider_exists;
                    if ($city->save()) {
                        $city->data = $response->json();
                        $response = json_encode($city, true);
                    } else
                        $response = json_encode('Error occured in backend system.');
                } else
                    $response = $response->body();
            } else {
                $response = response()->json($validator->messages(), 200);
            }
        } else $response = abort(403, 'You are not authenticated');
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return false|\Illuminate\Http\Client\Response|string
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $rules = [
            'city_name' => 'required|unique:App\Models\City,name',
            'api_token' => 'required|min:2|max:64',
            'user_id' => 'nullable|int'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            $response = Http::get('http://api.openweathermap.org/data/2.5/weather?q=' . $request->get('city_name') . '&appid=' . $request->get('api_token') . '&units=metric');
            if ($response->status() == 200) {
                $city = City::findOrFail($id);
                $this->authorize('update', $city);
                $city->api_key = $request->get('api_token');
                $city->name = $request->get('city_name');
                if ($user_id = $request->get('user_id')) $city->user_id = $user_id;
                if ($city->save()) {
                    $city->data = $response->json();
                    $response = json_encode($city, true);
                } else
                    $response = json_encode('Error occured in backend system.');
            } else
                $response = $response->body();
        } else {
            $response = response()->json($validator->messages(), 200);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $this->authorize('update', $city);
        $city->delete();
        return response([], 204);
    }
}

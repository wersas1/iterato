<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ShowAdmin extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function __invoke(Request $request)
    {
        if(Gate::allows('admin-actions')){
            return view('admin.panel');
        }
        else abort(403, 'You are not authorized for this action.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        return view('user.home', compact('user'));
    }

    /**
     * Function to store API key for the user
     */
    public function storeKey(Request $request)
    {
        $request->validate([
            'user_api_key' => 'required'
        ]);
        $user = Auth::user();
        $user->api_key = $request->get('user_api_key');
        $user->save();
        return redirect('/home')->with('status', 'Your API key was successfully saved');
    }
}

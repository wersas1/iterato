<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View|void
     * @throws AuthorizationException
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(30);
        if (Gate::allows('admin-actions')) return view('users.list', compact('users'));
        else return abort('403', 'You are not authorized for this action.');
    }

    /**
     * Function to assign values on edit /create
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function assignValues(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string|max:128',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
            'type' => 'integer|min:0|max:1',
        ]);

        $user->name = $request->get('name');
        if ($request->get('type')) {
            $user->type = $request->get('type');
        }
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View|void
     */
    public function create()
    {
        if (Gate::allows('admin-actions')) return view('users.create');
        else return abort('403', 'You are not authorized for this action.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector|void
     */
    public function store(Request $request)
    {
        if (Gate::allows('admin-actions')){
        $user = new User;
        $this->assignValues($request, $user);
        $user->save();
        return redirect('/users')->with('success', 'Successfully added a user');}
        else return abort('403', 'You are not authorized for this action.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View|void
     * @throws AuthorizationException
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('update', $user);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View|void
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('update', $user);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Redirector|void
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize('update', $user);
        $this->assignValues($request, $user);
        $user->save();
        return redirect('/users')->with('success', 'You have updated a user.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse|Redirector|void
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('update', $user);
        $cities = City::where('user_id', $user->id)->get();
        foreach ($cities as $city) {
            $city->delete();
        }
        $user->delete();
        return redirect('/users')->with('alert', 'You have deleted a user.');
    }
}

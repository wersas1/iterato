<?php

use App\Models\City;
use App\Models\User;

/**
 * @param $class
 * @param array $attributes
 * @param null $times
 * @return mixed
 */
function create($class, $attributes = [], $times = null)
{
    return getFactory($class, $attributes, $times);
}

/**
 * @param $class
 * @param array $attributes
 * @param null $times
 * @return mixed
 */
function make($class, $attributes = [], $times = null)
{
    return getFactory($class, $attributes, $times, 'make');
}

/**
 * @param $class
 * @param array $attributes
 * @param null $times
 * @return mixed
 */
function raw($class, $attributes = [], $times = null)
{
    return getFactory($class, $attributes, $times, 'raw');
}

/**
 * Determine which factory to use
 *
 * @param $class
 * @param array $attributes
 * @param null $times
 * @param string $method
 * @return mixed
 */
function getFactory($class = '', $attributes = [], $times = null, $method = 'create')
{

    if ($class === '\App\Models\User' || $class == 'App\Models\User') {
        $factory = User::factory($times)->count($times)->$method($attributes);
    } else {
        $factory = City::factory($times)->count($times)->$method($attributes);
    }

    return $factory;
}

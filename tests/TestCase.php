<?php

namespace Tests;

use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\App;
use Illuminate\Testing\TestResponse;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;


    /**
     * @var null
     */

    protected $base_model = null;
    /**
     * @var null
     */

    protected $base_route = null;
    /**
     * @var array
     */

    private $attributes = [];

    /**
     * @param null $user
     * @return TestCase
     */
    protected function signIn($user = null)
    {
        $user = $user ?? create(User::class);
        $this->actingAs($user);
        return $this;
    }

    /**
     * Base model setter
     *
     * @param $model
     */
    protected function setBaseModel($model)
    {
        $this->base_model = $model;
    }

    /**
     * Base route setter
     *
     * @param $route
     */
    protected function setBaseRoute($route)
    {
        $this->base_route = $route;
    }

    /**
     * Attributes setter
     *
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * Create specified resource in storage and check if is present
     *
     * @param $model
     * @param $route
     * @param array $attributes
     * @return TestResponse
     */
    protected function create($model = '', $route = '', $attributes = [])
    {
        $data = $this->manageData('store', $model, $route);
        $model = $data[0];
        $route = $data[1];
        $attributes = raw($model, $attributes);
        $response = $this->followingRedirects()->postJson(route($route), $attributes)->assertSuccessful()->assertDontSeeText('required');
        $model = new $model;
        if(isset($attributes['password'])&&isset($attributes['remember_token'])){
            unset($attributes['password']);
            unset($attributes['email_verified_at']);
            unset($attributes['remember_token']);
        }
        if(isset($attributes['city_name'])&&isset($attributes['api_token']))
        {
            $attributes['name']=$attributes['city_name'];
            unset($attributes['city_name']);
            $attributes['api_key']=$attributes['api_token'];
            unset($attributes['api_token']);
            unset($attributes['provider']);
        }
        $this->assertDatabaseHas($model->getTable(), $attributes);
        return $response;
    }

    /**
     * Update specified resource in storage and check if updated
     *
     * @param $model
     * @param $route
     * @param array $attributes
     * @return TestResponse
     */
    protected function update($model = '', $route = '', $attributes = [])
    {
        $data = $this->manageData('update', $model, $route);
        $model = $data[0];
        $route = $data[1];
        $model = create($model, $attributes);
        $model->save();
        $this->expectAuthException();
        tap($model->fresh(), function ($model) use ($attributes) {
            collect($attributes)->each(function ($value, $key) use ($model) {
                $this->assertEquals($value, $model[$key]);
            });
        });
        return $this->patchJson(route($route, $model->id), $attributes)->assertSuccessful();
    }

    /**
     * Remove specified resource from storage and check if it is missing
     *
     * @param string $model
     * @param string $route
     * @return TestResponse
     */
    protected function destroy($model = '', $route = '')
    {
        $this->manageData('destroy', $model, $route);
        $model = create($model);
        $this->expectAuthException();
        $response = $this->deleteJson(route($route, $model->id));
        $this->assertDatabaseMissing($model->getTable(), $model->toArray());
        return $response;
    }

    /**
     * Pass data through operators and disable exception handling
     *
     * @param $action
     * @param $model
     * @param $route
     * @return array
     */
    private function manageData($action, $model, $route)
    {
        $this->withoutExceptionHandling();
        //Ternary operator(if base route is set then store it, if not it is assigned route)
        $route = $this->base_route ? "{$this->base_route}" . '.' . $action : $route;
        //If base model is set, keep it if not assign model
        $model = $this->base_model ?? $model;
        return [$model, $route];
    }

    /**
     * Expect Authentication exception
     *
     * @return void
     */
    private function expectAuthException()
    {
        if (!auth()->user()) {
            $this->expectException(AuthorizationException::class);
        }
    }

    /**
     * Sign in as admin
     *
     * @return void
     */
    public function signInAsAdmin(){
        $admin=User::factory()->create();
        $admin->type=1;
        $this->signIn($admin);
    }
}

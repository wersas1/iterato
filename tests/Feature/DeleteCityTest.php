<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Session\TokenMismatchException;
use Tests\TestCase;

class DeleteCityTest extends TestCase
{
    /**
     * Setup testing env and assign route and model
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setBaseRoute('cities');
        $this->setBaseModel('App\Models\City');
    }

    /**
     * @test
     */
    public function privileged_user_can_delete_city(){
        $user=User::factory()->makeOne();
        $user->type=1;
        $user->save();
        $this->signIn($user);
        $this->destroy('App\Models\City','cities.destroy')->assertStatus(204);
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_city(){
        $this->followingRedirects()->destroy('App\Models\City','cities.destroy')->assertStatus(403);
    }

    /**
     * @test
     */
    public function unprivileged_user_cannot_delete_city(){
        $this->expectException(AuthorizationException::class);
        $this->signIn();
        $this->destroy('App\Models\City','cities.destroy')->assertStatus(204);
    }
}

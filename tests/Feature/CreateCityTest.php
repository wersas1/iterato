<?php

namespace Tests\Feature;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CreateCityTest extends TestCase
{
    /**
     * Setup testing env and assign route and model
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setBaseRoute('cities');
        $this->setBaseModel('\App\Models\City');
    }

    /**
     * @test
     */
    public function user_can_create_city()
    {
        $this->signIn();
        $this->create($this->base_model,$this->base_route,['city_name' => 'Minsk', 'api_token' => '697e26683e3d76e2e26e908d902c860a','user_id'=>Auth::user()->id]);
    }

    /**
     * @test
     */
    public function not_logged_in_user_cannot_create_city()
    {
        $this->post(route('cities.store'), ['name' => 'Minsk', 'api_key' => '697e26683e3d76e2e26e908d902c860a'])->
        assertSee('You are not authenticated')->
        assertStatus(403);
    }

    /**
     * @test
     */
    public function city_requires_api_key()
    {
        $this->signIn();
        $this->post(route('cities.store'), [])->assertSee('The api token field is required');
    }

    /**
     * @test
     */
    public function city_requires_name()
    {
        $this->signIn();
        $this->post(route('cities.store'), [])->assertSee('The city name field is required');
    }
}

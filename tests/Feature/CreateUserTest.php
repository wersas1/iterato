<?php

namespace Tests\Feature;

use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /**
     * Setup testing env and assign route and model
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setBaseRoute('users');
        $this->setBaseModel('\App\Models\User');
    }

    /**
     * @test
     */
    public function admin_can_create_user_on_admin_panel()
    {
        $this->signInAsAdmin();
        $this->create();
    }

    /**
     * @test
     */
    public function unauthorized_user_can_create_user_on_register()
    {
        $this->get(route('register'));
        $this->followingRedirects()->post(route('register'),
            [
                'password' => 'hackme123',
                'email' => 'johndoe@example.com'
            ])->assertStatus(200);
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_create_user_on_admin_panel()
    {
        $this->followingRedirects()->post(route('users.store'), [
            'name' => 'UserName',
            'password' => 'hackme123',
            'email' => 'johnis@example.com'
        ])->assertSee('You are not authorized')->assertStatus(403);
    }

    /**
     * @test
     */
    public function user_requires_name()
    {
        $this->signInAsAdmin();
        $this->followingRedirects()->post(route('users.store'),
            [
                'name' => '',
                'password' => 'hackme123',
                'email' => 'johnis123@example.com'
            ],['Content-Type'=>'multipart/form-data;','Accept'=>'application/json'])->assertJsonValidationErrors('name');
    }

    /**
     * @test
     */
    public function user_requires_password()
    {
        $this->signInAsAdmin();
        $this->followingRedirects()->post(route('users.store'),
            [
                'name' => 'UserName',
                'password' => '',
                'email' => 'johnis12345@example.com'
            ],['Content-Type'=>'multipart/form-data;','Accept'=>'application/json'])->assertJsonValidationErrors('password');
    }

    /**
     * @test
     */
    public function user_requires_email()
    {
        $this->signInAsAdmin();
        $this->followingRedirects()->post(route('users.store'),
            [
                'name' => 'UserName',
                'password' => 'hackme123',
                'email' => ''
            ],['Content-Type'=>'multipart/form-data;','Accept'=>'application/json'])->assertJsonValidationErrors('email');
    }
}

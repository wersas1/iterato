<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /**
     * Setup testing env and assign route and model
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setBaseRoute('users');
        $this->setBaseModel('App\Models\User');
    }

    /**
     * @test
     */
    public function privileged_user_can_delete_user(){
        $user=User::factory(['type'=>1])->makeOne();
        $this->signIn($user);
        $this->followingRedirects()->destroy('App\Models\User','users.destroy')->assertSee('deleted');
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_delete_user(){
        $this->followingRedirects()->destroy('App\Models\User','users.destroy')->assertStatus(403);
    }

    /**
     * @test
     */
    public function unprivileged_user_cannot_delete_user(){
        $this->expectException(AuthorizationException::class);
        $this->signIn();
        $this->followingRedirects()->destroy('App\Models\User','users.destroy')->assertStatus(403);
    }
}

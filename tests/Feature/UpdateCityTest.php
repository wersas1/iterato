<?php

namespace Tests\Feature;

use App\Models\City;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class UpdateCityTest extends TestCase
{

    use WithFaker;

    /**
     * @var array
     */
    private $attributes = [];

    /**
     * Setup testing env and assign route and model
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setBaseRoute('cities');
        $this->setBaseModel('\App\Models\City');
    }

    /**
     * @test
     */
    public function authorized_user_can_update_city()
    {
        $user=User::factory()->makeOne();
        $user->type=1;
        $user->save();
        $city=City::factory()->makeOne();
        $city->save();
        $this->signIn($user);
        $this->patch(route('cities.update', $city->id), ['city_name' => 'Warsaw', 'api_token' => '9af65e7be06e2c5ca71be6b63bf25009'])->assertSee('updated');
    }


    /**
     * @test
     */
    public function unauthorized_user_cannot_update_city()
    {
        $user=User::factory()->makeOne();
        $user->type=1;
        $user->save();
        $this->signIn($user);
        $user_id=$user->id;
        $name='Berlynas';
        $api_key='965a5f787db983535b611896bd1c2c88';
        $city=City::factory(compact('user_id','name','api_key'))->create();
        $this->signIn();
        $this->patch(route('cities.update', $city->id), ['city_name' => 'Warsaw', 'api_token' => '9af65e7be06e2c5ca71be6b63bf25009'])->assertStatus(403);
    }

    /**
     * @test
     */
    public function cannot_update_city_if_wrong_name()
    {
        $user=User::factory()->makeOne();
        $user->type=1;
        $user->save();
        $this->signIn($user);
        $this->patch(route('cities.update', '1'), ['city_name' => 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW', 'api_token' => '9af65e7be06e2c5ca71be6b63bf25009'])->assertSee('city not found');
    }

}

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use WithFaker;

    /**
     * @var array
     */
    private $attributes = [];

    /**
     * Setup testing env and assign route and model
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setBaseRoute('users');
        $this->setBaseModel('\App\Models\User');
    }

    /**
     * @test
     */
    public function authorized_user_can_update_user()
    {
        $user = User::factory()->makeOne();
        $user->type = 1;
        $user->save();
        $this->signIn($user);
        $this->followingRedirects()->patch(route('users.update', $user->id), [
            'name' => 'ExampleName',
            'password' => '9af65e7be06e2c5ca71be6b63bf25009',
            'type' => 1,
            'email' => 'newEmailExample@example.com'
        ])->assertSee('updated');
    }


    /**
     * @test
     */
    public function unauthorized_user_cannot_update_user()
    {
        $this->signIn();
        $this->followingRedirects()->patch(route('users.update', 1), [
            'name' => 'ExampleName',
            'password' => '9af65e7be06e2c5ca71be6b63bf25009',
            'type' => 1,
            'email' => 'newEmailExample@example.com'
        ])->assertStatus(403);
    }

}

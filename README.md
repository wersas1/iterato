## About Project

**Author:** 

Vincentas Baubonis


**Description:**

This project is a simple weathers page for company Iterato. The page has two main parts - form and tabs with
weathers. The form has two text inputs (token and city) and submit button. Tabs
contain only basic information of the city's weather forecast.


**Tools and libraries:**

Bootstrap 4 (http://getbootstrap.com/);

jQuery (http://jquery.com/) / JavaScript Fetch API;

PHP framework (Laravel) for communication between weather API and page;


**Weather API:**

http://openweathermap.org/current



**Application flow:**

● The API key should entered into first input field.

Example of API key: 44db6a862fba0b067b1930Da0d769e98

● The city name should be entered into second input field.

● The submit button is green on right side, it should be clicked when fields are filled in.

● The form’s data is submitted via AJAX to back end system and user can see a new tab added.

● Backend system calls to external data provider.

● The response of external data is passed to front-end system as general data format.

● The possibility to add other providers was investigated but not yet finished. 

● Each new city is shown as new tab with data inside.


**Testing:**

Run automated application tests through command line interface with: php artisan test
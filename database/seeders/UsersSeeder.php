<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'type' => 1,
            'email' => 'admin@example.com',
            'password' => bcrypt('admin123'),
            'created_at'=>Carbon::now(),
        ]);

      User::factory(35)->create();
    }
}

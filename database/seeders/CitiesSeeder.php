<?php

namespace Database\Seeders;

use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // City::factory(5)->create();
        DB::table('cities')->insert([
            'name' => 'Vilnius',
            'api_key'=>'965a5f787db983535b611896bd1c2c88',
            'created_at'=>Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'name' => 'Kaunas',
            'api_key'=>'965a5f787db983535b611896bd1c2c88',
            'created_at'=>Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'name' => 'Klaipėda',
            'api_key'=>'965a5f787db983535b611896bd1c2c88',
            'created_at'=>Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'name' => 'London',
            'api_key'=>'965a5f787db983535b611896bd1c2c88',
            'created_at'=>Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'name' => 'Moscow',
            'api_key'=>'965a5f787db983535b611896bd1c2c88',
            'created_at'=>Carbon::now(),
        ]);
    }
}

<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = City::class;

    /**
     * @var string
     */
    protected $api_key = '965a5f787db983535b611896bd1c2c88';

    protected $cities = [
        'Los Angeles', 'Washington', 'Moscow', 'London', 'Seattle', 'New Yorkshire', 'Švenčionys', 'Pabradė', 'Roterdam',
        'Amsterdam', 'Eindhoven', 'Klaipėda', 'Kaunas', 'Vilnius', 'Palanga', 'Nida', 'Alanija', 'Prague', 'Birmingham',
        'Šiauliai', 'Panevėžys'
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->cities[mt_rand(0, 20)],
            'user_id' => null,
            'api_key' => $this->api_key,
            'provider' => 'api.openweathermap.com',
        ];
    }
}
